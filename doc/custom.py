import sys
import os
import zipfile
import subprocess
try: from urllib.request import urlretrieve
except: from urllib import urlretrieve

def exit():
	os.system("rm __pycache__ *.pyc *.py~ -f -r")
	sys.exit()

def download_extract(h, f, m):
	urlretrieve(h, f)
	with zipfile.ZipFile(f) as zf:
		zf.extractall(m)
	os.remove(f)
	o = os.listdir('./'+m)[0]+'/'
	os.chdir(m+'/'+o)
	FNULL = open(os.devnull, 'w')
	retcode = subprocess.call(['python', 'setup.py', 'install'], stdout=FNULL, stderr=subprocess.STDOUT)
	os.chdir("../../")

def progressbar(t, pfx = "", size = 60):
    count = len(t)
    def _show(_i):
        x = int(size*_i/count)
        sys.stdout.write("%s[%s%s] %i/%i\r" % (pfx, "*"*x, "."*(size-x), _i, count))
        sys.stdout.flush()
    
    _show(0)
    for i, item in enumerate(t):
        yield item
        _show(i+1)
    sys.stdout.write("\n")
    sys.stdout.flush()
