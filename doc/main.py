import sys
import getpass
import shutil
import os
import time

#CUSTOM MODULES
import arpmonitor
import lanscan
import portscan
import info
import text
import custom
from scapy.all import *
if getpass.getuser() == "root":
	pass
else:
        print ("[-] Run again as root.")
        custom.exit()

print (text.logo)

op = 0
for nt in info.modules:
	try:
		__import__(info.modules[nt])
	except:
		op+=1
		
x = 'modules' 
if op < len(info.modules):
	if os.path.exists(x):
		pass		
	else:
		os.mkdir(x, 755)
	os.chdir(x)
	for g in custom.progressbar(info.paths, " [*] Installing modules : ", 30):
		custom.download_extract(info.paths[g], info.modules[g]+".zip",info.modules[g])

else:
	pass

if len(sys.argv) < 2:
	print(text.usage)
	custom.exit()
else:
	pass


if sys.argv[1] == "lanscan":
	try:
		lanscan.run(sys.argv[2])
	except KeyboardInterrupt:
		custom.exit()
elif sys.argv[1] == "arpmonitor":
	    
	def run(pkt):
    		if pkt[ARP].op == 1:
        		print "[*] REQUEST: " + pkt[ARP].psrc + " --> " + pkt[ARP].pdst
    		if pkt[ARP].op == 2:
        		print "[*] RESPONSE: " + pkt[ARP].hwsrc + "address " + pkt[ARP].psrc
 

elif sys.argv[1] == "portscan":
	pass
else:
	pass

custom.exit()
