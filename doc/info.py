import lanscan
import portscan
import arpmonitor
from scapy.all import *
paths = ["https://pypi.python.org/packages/source/n/netaddr/netaddr-0.7.12.zip", "http://www.secdev.org/projects/scapy/files/scapy-latest.zip", "https://pypi.python.org/packages/source/p/pysmb/pysmb-1.1.13.zip#md5=9032f8069159672ef0d7c5ae3d22f712"]
version = 2.3
modules = ["netaddr", "scapy"]
cmdl = {'lanscan': lanscan.run, 'portscan': portscan.run, 'arpmonitor':sniff}
