try:
	from netaddr import *
except:
	pass
import re
import os
def run(ip):
	net = IPNetwork(ip)
	print (' [*] Going to scan %s IP Addresses\n' % len(net))
	uphosts = []
	lifeline = re.compile(r"(\d) received")
	report = ("No response","Partial Response","Alive")
	for host in net:
		ping = os.popen("ping -q -c2 -w2 "+str(host),"r")
		while 1:
			line = ping.readline()
			if not line: break
			got = re.findall(lifeline,line)
			if "2" in got:
				uphosts.append(host)
				print (" [*] ALIVE %s"%(host))
				break
